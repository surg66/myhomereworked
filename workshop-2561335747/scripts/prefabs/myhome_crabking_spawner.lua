--Note: fake prefab, for "crabking_spawner"
--      without tag CLASSIFIED if restricted area mode
local function fn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddNetwork()

    inst:AddTag("crabking_spawner_restricted")

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end

    inst.persists = false

    return inst
end

return Prefab("myhome_crabking_spawner", fn)
