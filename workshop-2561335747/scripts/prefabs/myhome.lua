local function GetAvailablePointInRadius(x, z, radius, homeindex, physicsradius, mindistwall)
    local degrees = {}
    for i = 1, 360 do
        table.insert(degrees, (i - 1))
    end
    local numdegree = #degrees
    while numdegree > 0 do
        local index = math.random(1, numdegree)
        local angle = degrees[index] * PI / 180
        local dist = radius + (physicsradius or 0)
        local tx = x + (math.sin(angle) * dist)
        local tz = z + (math.cos(angle) * dist)

        if mindistwall ~= nil then
            local foundents = TheSim:FindEntities(tx, 0, tz, mindistwall, {"mh_h"..homeindex})
            if foundents[1] ~= nil then
                tx = x + (math.sin(angle) * (dist + mindistwall))
                tz = z + (math.cos(angle) * (dist + mindistwall))
            end
        end

        if TheWorld.Map:IsAboveGroundAtPoint(tx, 0, tz) and
           TheWorld.net.components.myhome_manager:GetHomeByPosition(tx, 0, tz) == nil then
            return Vector3(tx, 0, tz)
        end

        table.remove(degrees, index)
        numdegree = #degrees
    end
    return nil
end

local function IsPointInside(inst, x, y, z, isrect, outdist)
    if y <= TUNING.MYHOME.HOME_HEIGHT then
        if isrect and inst._rect ~= nil then
            local dist = outdist or 0
            if x >= inst._rect[1] - dist and x <= inst._rect[3] + dist and
               z >= inst._rect[2] - dist and z <= inst._rect[4] + dist then
                return true
            end
        elseif inst._polygon ~= nil and TheSim:WorldPointInPoly(x, z, inst._polygon) then
            return true
        end
    end
    return false
end

local function IsAvailableForDoer(inst, doer)
    if TUNING.MYHOME.ADMIN and doer.userid ~= nil and doer.Network:IsServerAdmin() then
        return true
    end

    local myhome_manager = TheWorld.net.components.myhome_manager
    local userindex = myhome_manager:GetUserIndexByPlayer(doer)

    if userindex > 0 and (userindex == inst._ownerindex or myhome_manager:IsFriend(inst._ownerindex, userindex)) then
        return true
    end

    -- for followers: shadowwaxwell, pigman etc.
    if doer.userid == nil and doer.components.follower then
        local leader = doer.components.follower:GetLeader()
        if leader then
            if TUNING.MYHOME.ADMIN and leader.Network:IsServerAdmin() then
                return true
            end
            userindex = myhome_manager:GetUserIndexByPlayer(leader)
            if userindex > 0 and (userindex == inst._ownerindex or myhome_manager:IsFriend(inst._ownerindex, userindex)) then
                return true
            end
        end
    end

    return false
end

local function IsProcessDestroy(inst)
    return inst._isprocessdestroy
end

local function DoTryPush(inst, ent)
    local cmptrackable = ent.components.myhome_trackable

    if cmptrackable and not cmptrackable:OnCanPush(inst) then
        return false
    end

    local ex, ey, ez = ent.Transform:GetWorldPosition()
    local x, _, z = inst.Transform:GetWorldPosition()
    local mindistwall = cmptrackable and cmptrackable.mindistwall or 4
    local optfx = cmptrackable and cmptrackable.optfx or {old = nil, new = nil}
    local pos = GetAvailablePointInRadius(x, z, inst._radius, inst._homeindex, ent:GetPhysicsRadius(0), mindistwall)

    if pos ~= nil then
        local has_inventoryowner = false
        if ent.components.inventoryitem then
            local owner = ent.components.inventoryitem.owner
            if owner ~= nil then
                has_inventoryowner = true
                if owner.components.inventory ~= nil then
                    owner.components.inventory:DropItem(ent)
                elseif owner.components.container ~= nil then
                    owner.components.container:DropItem(ent)
                end
            end
        end

        if ent.Physics ~= nil then
            ent.Physics:Teleport(pos.x, pos.y, pos.z)
        else
            ent.Transform:SetPosition(pos.x, pos.y, pos.z)
        end

        local fx = nil
        if ent:HasTag("player") then
            -- old pos
            fx = SpawnPrefab("spawn_fx_medium")
            if fx then
                fx.Transform:SetPosition(ex, ey, ez)
            end
            -- new pos
            fx = SpawnPrefab("spawn_fx_medium")
            if fx then
                fx.Transform:SetPosition(pos.x, pos.y, pos.z)
            end
        else
            -- old pos
            if optfx.old ~= nil and not has_inventoryowner then
                fx = SpawnPrefab(optfx.old)
                if fx then
                    fx.Transform:SetPosition(ex, ey, ez)
                end
            end
            -- new pos
            if optfx.new ~= nil then
                fx = SpawnPrefab(optfx.new)
                if fx then
                    fx.Transform:SetPosition(pos.x, pos.y, pos.z)
                end
            end
        end

        if ent:HasTag("player") and not ent:HasTag("playerghost") and
           ent.sleepingbag ~= nil and ent.sleepingbag.components.sleepingbag ~= nil then
            ent.sleepingbag.components.sleepingbag:DoWakeUp(false)
        end

        if cmptrackable then
            cmptrackable:OnSuccessPush(Vector3(ex, ey, ez), Vector3(pos.x, pos.y, pos.z))
        end

        return true
    end
    --fail, not found available point in radius
    if cmptrackable ~= nil then
        cmptrackable:OnFailPush(inst, Vector3(ex, ey, ez))
    end

    return false
end

local function DoPushForce(inst, ent)
    if ent ~= nil then
        local x, y, z = ent.Transform:GetWorldPosition()
        if inst:IsPointInside(x, y, z) then
            DoTryPush(inst, ent)
        end
    end
end

local function DoShowCtrl(inst)
    local x, y, z = inst.Transform:GetWorldPosition()
    local ents = TheSim:FindEntities(x, y, z, inst._radius, {"mh_h"..inst._homeindex})
    for _, v in ipairs(ents) do
        if v:HasTag("mh_ctrl") then
            if v._ctrlfx == nil then
                v._ctrlfx = SpawnPrefab("myhome_marker")
                if v._ctrlfx ~= nil then
                    v.components.myhome_wall:SetModeColor(1)
                    v._ctrlfx.entity:SetParent(v.entity)
                    v._ctrlfx.Transform:SetPosition(0, 1.5, 0)
                    v._ctrlfx:SetIcon(7)
                    v._ctrlfxtask = v:DoTaskInTime(2, function(ent)
                        ent._ctrlfx = nil
                        ent._ctrlfxtask = nil
                        ent.components.myhome_wall:SetModeColor(0)
                    end)
                end
            end
            break
        end
    end
end

local function DoDestroy(inst, doer, force)
    if inst._isprocessdestroy then return end

    inst._isprocessdestroy = true

    if doer ~= nil and doer:IsValid() then
        doer:AddTag("mh_captor")
        doer.components.talker:Say(STRINGS.MYHOME.REQUEST_DESTROY, 3)
    end

    local x, y, z = inst.Transform:GetWorldPosition()
    local ents = TheSim:FindEntities(x, y, z, inst._radius, {"mh_h"..inst._homeindex})
    if force then
        for _, v in ipairs(ents) do
            if v ~= nil and v:IsValid() then
                v.components.myhome_wall:UnLock()
                v.components.myhome_wall:SetModeColor(0)
            end
        end
        if doer ~= nil and doer:IsValid() then
            doer:RemoveTag("mh_captor")
            doer.components.talker:Say(STRINGS.MYHOME.PROCESS_DESTROYED, 3)
            if TUNING.MYHOME.ANNOUNCE then
                TheNet:Announce(string.format(STRINGS.MYHOME.ANNOUNCE_DESTROYED, doer:GetDisplayName()))
            end
        end
        inst:Remove()
    else
        for _, v in ipairs(ents) do
            if v ~= nil and v:IsValid() and v:HasTag("mh_ctrl") then
                if v._ctrlfx ~= nil and v._ctrlfxtask ~= nil then
                    v._ctrlfx:Remove()
                    v._ctrlfxtask:Cancel()
                    v._ctrlfx = nil
                    v._ctrlfxtask = nil
                    v.components.myhome_wall:SetModeColor(0)
                end
                v._ctrlfx = SpawnPrefab("myhome_marker")
                if v._ctrlfx ~= nil then
                    v.components.myhome_wall:SetModeColor(1)
                    v._ctrlfx.entity:SetParent(v.entity)
                    v._ctrlfx.Transform:SetPosition(0, 1.5, 0)
                    v._ctrlfx:SetIcon(8)
                    v._ctrlfxtask = v:DoTaskInTime(2, function(ent)
                        ent._ctrlfx = nil
                        ent._ctrlfxtask = nil
                        ent.components.myhome_wall:SetModeColor(0)
                    end)
                end
                break
            end
        end

        --pass flash 1
        inst:DoTaskInTime(1, function(ent)
            for _, v in ipairs(ents) do
                if v ~= nil and v:IsValid() then
                    v.components.myhome_wall:SetModeColor(2)
                end
            end
        end)
        --pass flash 2
        inst:DoTaskInTime(1.5, function(ent)
            for _, v in ipairs(ents) do
                if v ~= nil and v:IsValid() then
                    v.components.myhome_wall:SetModeColor(1)
                end
            end
        end)

        inst:DoTaskInTime(2, function(ent)
            for _, v in ipairs(ents) do
                if v ~= nil and v:IsValid() then
                    v.components.myhome_wall:UnLock()
                    v.components.myhome_wall:SetModeColor(0)
                end
            end
            if doer ~= nil and doer:IsValid() then
                doer:RemoveTag("mh_captor")
                doer.components.talker:Say(STRINGS.MYHOME.PROCESS_DESTROYED, 3)
                if TUNING.MYHOME.ANNOUNCE then
                    TheNet:Announce(string.format(STRINGS.MYHOME.ANNOUNCE_DESTROYED, doer:GetDisplayName()))
                end
            end
            ent:Remove()
        end)
    end
end

local function GetPointOutside(inst, physicsradius, mindistwall)
    local x, _, z = inst.Transform:GetWorldPosition()
    return GetAvailablePointInRadius(x, z, inst._radius, inst._homeindex, physicsradius, mindistwall)
end

local function GetCtrl(inst)
    local x, y, z = inst.Transform:GetWorldPosition()
    local ents = TheSim:FindEntities(x, y, z, inst._radius, {"mh_h"..inst._homeindex})

    for _, v in ipairs(ents) do
        if v:HasTag("mh_ctrl") then
            return v
        end
    end

    return nil
end

local function SetTesting(inst, allow)
    if allow == true then
        inst._allowedtotest = true
    else
        inst._allowedtotest = nil
    end
end

local function SetData(inst, data)
    if data ~= nil then
        if data.radius then
            inst._radius = data.radius
        end

        if data.rect then
            inst._rect = data.rect
        end

        if data.polygon then
            inst._polygon = data.polygon
        end

        if data.ownerindex then
            inst._ownerindex = data.ownerindex
        end

        if data.homeindex then
            inst._homeindex = data.homeindex
        end
        if data.buildtime then
            inst._buildtime = data.buildtime
        end
    end
end

local function OnUpdate(inst)
    if inst._allowedtotest ~= true then
        return
    end

    local x, y, z = inst.Transform:GetWorldPosition()
    local found_ents = TheSim:FindEntities(x, y, z, inst._radius, {"mh_tracked"}, {"mh_trackedpause"})

    if #found_ents == 0 then
        return
    end

    local inside_ents = {}

    for _, ent in ipairs(found_ents) do
        local ex, ey, ez = ent.Transform:GetWorldPosition()
        if inst:IsPointInside(ex, ey, ez) then
            table.insert(inside_ents, ent)
        end
    end

    for _, ent in ipairs(inside_ents) do
        if ent:HasTag("player") then
            if TUNING.MYHOME.INTRUDERS and not ent:HasTag("playerghost") and not inst:IsAvailableForDoer(ent) then
                if DoTryPush(inst, ent) then
                    if ent.components.talker ~= nil then
                        ent.components.talker:Say(STRINGS.MYHOME.THIS_IS_HOME_ANOTHER, 3)
                    end
                end
            end
        else
            DoTryPush(inst, ent)
        end
    end
end

local function OnInit(inst)
    TheWorld:PushEvent("myhome_register", inst)
end

local function OnSave(inst, data)
    data.radius = inst._radius or 0
    data.rect = inst._rect or nil
    data.polygon = inst._polygon or nil
    data.ownerindex = inst._ownerindex or 0
    data.homeindex = inst._homeindex or 0
end

local function OnLoad(inst, data)
    if data ~= nil then
        inst._radius = data.radius or 0
        inst._rect = data.rect or nil
        inst._polygon = data.polygon or nil
        inst._ownerindex = data.ownerindex or 0
        inst._homeindex = data.homeindex or 0
    end
end

local function OnRemoveEntity(inst)
    TheWorld:PushEvent("myhome_unregister", inst)
end

local function fn()
    local inst = CreateEntity()

    --[[Non-networked entity]]
    inst.entity:SetCanSleep(false)
    inst.persists = true

    inst.entity:AddTransform()
    inst.entity:AddAnimState()

    inst:AddTag("FX")
    inst:AddTag("NOCLICK")
    inst:AddTag("myhome")

    inst._radius = 0
    inst._homeindex = 0
    inst._ownerindex = 0
    inst._isprocessdestroy = false

    inst.IsPointInside = IsPointInside
    inst.IsAvailableForDoer = IsAvailableForDoer
    inst.IsProcessDestroy = IsProcessDestroy
    inst.DoPushForce = DoPushForce
    inst.DoShowCtrl = DoShowCtrl
    inst.DoDestroy = DoDestroy
    inst.GetPointOutside = GetPointOutside
    inst.GetCtrl = GetCtrl
    inst.SetTesting = SetTesting
    inst.SetData = SetData

    inst:DoTaskInTime(0, OnInit)
    inst:DoPeriodicTask(2, OnUpdate)

    inst.OnSave = OnSave
    inst.OnLoad = OnLoad
    inst.OnRemoveEntity = OnRemoveEntity

    return inst
end

return Prefab("myhome", fn)
