local function IsNearOceanIce(x, y, z)
    local tilex, tiley = TheWorld.Map:GetTileCoordsAtPoint(x, y, z)
    local cx, cy, cz = TheWorld.Map:GetTileCenterPoint(tilex, tiley)

    if TheWorld.Map:IsOceanIceAtPoint(cx - 8, cy, cz) then return true end
    if TheWorld.Map:IsOceanIceAtPoint(cx + 8, cy, cz) then return true end
    if TheWorld.Map:IsOceanIceAtPoint(cx, cy, cz - 8) then return true end
    if TheWorld.Map:IsOceanIceAtPoint(cx, cy, cz + 8) then return true end
    if TheWorld.Map:IsOceanIceAtPoint(cx - 8, cy, cz - 8) then return true end
    if TheWorld.Map:IsOceanIceAtPoint(cx - 8, cy, cz + 8) then return true end
    if TheWorld.Map:IsOceanIceAtPoint(cx + 8, cy, cz - 8) then return true end
    if TheWorld.Map:IsOceanIceAtPoint(cx + 8, cy, cz + 8) then return true end

    return false
end

local function OnUpdate(inst)
    inst:Hide()

    local isadmin = false
    local x, y, z = inst.Transform:GetWorldPosition()

    if TheWorld.Map:GetPlatformAtPoint(x, y, z) then
        return inst:Show()
    end

    if TheWorld.Map:IsOceanIceAtPoint(x, y, z) then
        return inst:Show()
    elseif TheWorld.Map:IsVisualGroundAtPoint(x, y, z) then
        if IsNearOceanIce(x, y, z) then
            return inst:Show()
        end
    end

    local client = TheNet:GetClientTableForUser(TheNet:GetUserID())
    if TUNING.MYHOME.ADMIN and client and client.admin then
        isadmin = true
    end

    if not isadmin then
        --check restricted prefabs
        if TUNING.MYHOME.RESTRICTED_AREA then
            local ents = TheSim:FindEntities(x, y, z, 16, {"mh_r3"})
            if ents[1] ~= nil then
                return inst:Show()
            else
                ents = TheSim:FindEntities(x, y, z, 8, {"mh_r2"})
                if ents[1] ~= nil then
                    return inst:Show()
                end
            end
        end

        --check min distance any lock wall
        if TUNING.MYHOME.MIN_DIST_BETWEEN_MYHOMES > 0 then
            local ents = TheSim:FindEntities(x, y, z, TUNING.MYHOME.MIN_DIST_BETWEEN_MYHOMES, {"mh_lock"})
            if ents[1] ~= nil then
                return inst:Show()
            end
        end

        --check distance not friendly lock wall
        if ThePlayer.myhome_userindex and ThePlayer.myhome_userindex > 0 then
            for _, v in ipairs(TheSim:FindEntities(x, 0, z, 15, {"mh_lock"})) do
                if v.components.myhome_wall.ownerindex > 0 and v.components.myhome_wall.ownerindex ~= ThePlayer.myhome_userindex and
                   not TheWorld.net.components.myhome_manager:IsFriend(v.components.myhome_wall.ownerindex, ThePlayer.myhome_userindex) then
                    return inst:Show()
                end
            end
        end

        --check topology
        for i, node in ipairs(TheWorld.topology.nodes) do
            if TheSim:WorldPointInPoly(x, z, node.poly) then
                local id = TheWorld.topology.ids[i]
                if not TUNING.MYHOME.ACCESS_RUINS_AREA then
                    if id:find("TheLabyrinth") then
                        return inst:Show()
                    end
                    if id:find("Military") then
                        return inst:Show()
                    end
                    if id:find("MilitaryPits") then
                        return inst:Show()
                    end
                    if id:find("Sacred") then
                        return inst:Show()
                    end
                    if id:find("SacredAltar") then
                        return inst:Show()
                    end
                    if id:find("MoreAltars") then
                        return inst:Show()
                    end
                    if id:find("AtriumMaze") then
                        return inst:Show()
                    end
                end

                if not TUNING.MYHOME.ACCESS_HERMITCRAB_ISLAND and id:find("HermitcrabIsland") then
                    return inst:Show()
                end

                if not TUNING.MYHOME.ACCESS_ARCHIVE_MAZE then
                    if id:find("ArchiveMazeEntrance") then
                        return inst:Show()
                    end
                    if id:find("ArchiveMazeRooms") then
                        return inst:Show()
                    end
                end
            end
        end
    end
end

local function fn()
    local inst = CreateEntity()
    --[[Non-networked entity]]
    inst.entity:AddTransform()
    inst.entity:AddAnimState()

    inst.AnimState:SetBank("myhome_markers")
    inst.AnimState:SetBuild("myhome_markers")
    inst.AnimState:PlayAnimation("marker_placer")
    inst.AnimState:OverrideSymbol("swap_iconsmall", "myhome_markers", "icon2")

    inst:AddTag("FX")
    inst:AddTag("NOCLICK")

    inst.entity:SetPristine()

    inst:DoPeriodicTask(FRAMES, OnUpdate)

    if not TheWorld.ismastersim then
        return inst
    end

    inst.persists = false

    return inst
end

return Prefab("myhome_placemarker", fn)
