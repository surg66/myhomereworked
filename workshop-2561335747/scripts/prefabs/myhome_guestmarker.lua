local function SetPercent(inst, percent)
    inst.AnimState:SetPercent("anim", 1 - percent)
end

local function fn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddNetwork()

    inst.AnimState:SetBank("myhome_guestmarker")
    inst.AnimState:SetBuild("myhome_guestmarker")
    inst.AnimState:PlayAnimation("anim")
    inst.AnimState:AnimateWhilePaused(false)
    inst.AnimState:SetPercent("anim", 1)
    inst.AnimState:SetOrientation(ANIM_ORIENTATION.OnGround)
    inst.AnimState:SetLayer(LAYER_BACKGROUND)
    inst.AnimState:SetSortOrder(3)
    inst.AnimState:SetFinalOffset(1)

    inst:AddTag("DECOR")
    inst:AddTag("NOCLICK")

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end

    inst.persists = false

    inst.SetPercent = SetPercent

    return inst
end

return Prefab("myhome_guestmarker", fn)
