local wall_prefabs =
{
    "fence",
    "fence_gate",
}

local restricted_prefabs =
{
    --#1, check inside area
    {
        "migration_portal",
        "cave_hole",        
        "rock_moon_shell",
        "moonrockseed",
        "gravestone",
        "fissure_lower",
        "mound",
        "pond",
        "pond_mos",
        "pond_cave",
        "lava_pond",
        "statueglommer",
        "ancient_altar",
        "ancient_altar_broken",
        "sculpture_rookbody",
        "sculpture_bishopbody",
        "sculpture_knightbody",
        "sculpture_rooknose",
        "sculpture_bishophead",
        "sculpture_knighthead",
        "statueharp_hedgespawner",
        "charlie_lecturn",
        "nightmarelight",
        "rocky",
        "daywalker",
        "daywalker_pillar",
        "minotaur",
        "toadstool",
        "dragonfly",
        "bearger",
        "klaus",
        "deerclops",
        "sharkboi",
        "moose",
        "mooseegg",
        "moose_nesting_ground",
        "mossling",
        "klaus_sack",
        "stagehand",
        "tallbirdnest",
        "deer",
        "deerlantler",
        "deer_red",
        "deer_blue",
        "atrium_gate",
        "pandoraschest",
        "minotaurchest",
        "sacred_chest",
        "carnival_host",
        "carnival_crowkid",
        "saltstack",
        "waterplant",

        --Moon island
        "moonspiderden",
        "hotspring",
        "moon_altar_icon",
        "moon_altar_ward",
        "moon_altar_crown",
        "moon_altar_rock_glass",
        "moon_altar_rock_seed",
        "moon_altar_rock_idol",
        "moon_altar_glass",
        "moon_altar_seed",
        "moon_altar_idol",
        "moon_altar",
        "moon_altar_cosmic",
        "moon_altar_astral",
        "moon_altar_link",
        "moon_device_construction1",
        "moon_device_construction2",
        "moon_device_construction3",
        "alterguardian_phase1",
        "alterguardian_phase1dead",
        "alterguardian_phase2",
        "alterguardian_phase2dead",
        "alterguardian_phase3",
        "alterguardian_phase3dead",

        --Hermit island
        "hermithouse_construction1",
        "hermithouse_construction2",
        "hermithouse_construction3",
        "hermithouse",
        "hermitcrab",
        "beebox_hermit",
        "meatrack_hermit",
        "moon_fissure_plugged",

        --Grotto
        "grotto_pool_big",
        "grotto_pool_small",
        "fissure_grottowar",
        "nightmaregrowth",

        --Archive maze
        "archive_cookpot",
        "archive_lockbox_dispencer",
        "archive_security_desk",
        "archive_switch",
        "archive_switch_base",
        "archive_switch_pad",
        "archive_orchestrina_main",
        "archive_orchestrina_small",
        "archive_orchestrina_base",
        "dustmothden",

        --Etc
        "wagstaff_npc",
        "atrium_key",
        "hermit_pearl",
        "hermit_cracked_pearl",
        "terrarium",

        --ToDo: events prefabs
    },
    --#2, check outside area
    {
        "cave_entrance",
        "cave_entrance_open",
        "cave_exit",
        "wormhole",
        "walrus_camp",
        "toadstool_cap",
        "tentacle_pillar",
        "tentacle_pillar_hole",
        "pigtorch",
    },
    --#3, check outside area, big distance
    {
        "multiplayer_portal",
        "multiplayer_portal_moonrock_constr",
        "multiplayer_portal_moonrock",
        "spawnpoint_master",
        "resurrectionstone",
        "critterlab",
        "moonbase",
        "oasislake",
        "antlion",
        "beequeenhivegrown",
        "beequeenhive",
        "pigking",
        "charlie_stage_post",
        "junk_pile_big",
        --Note: "antlion_spawner", "crabking_spawner" can't be restricted, because has tag CLASSIFIED
        --      so, mastersim spawned "myhome_antlion_spawner", "myhome_crabking_spawner" instead of him
        "myhome_antlion_spawner",
        "myhome_crabking_spawner",

        --Monkey island
        "monkeyqueen",
        "monkeyisland_portal",
    },
    --#4, check inside area, if count walls < 60
    {
        "reeds",
        "beehive",
        "wasphive",
        "lichen",
        "cactus",
        "oasis_cactus",
        "rabbithole",
        "beefalo",
        "babybeefalo",
        "lightninggoat",
        "moon_fissure",
        "wobster_den",
        "spiderhole",
        "batcave",
        "slurtlehole",
        "monkeybarrel",
        "houndmound",
        "catcoonden",
    },
}

local coming_bosses_prefabs =
{
    "bearger",
    "deerclops",
    "toadstool",
    "minotaur",
    "stalker_atrium",
}

local coming_mobs_prefabs =
{
    "krampus",
    "worm",
    "hound",
    "mutatedhound",
    "firehound",
    "icehound",
    "warglet",
}

local hauntable_mobs_prefabs =
{
    "warg",
    "claywarg",
    "spat",
    "koalefant_summer",
    "koalefant_winter",
}

local klaussack_prefabs =
{
    "klaus_sack",
    "klaus",
    "deer",
    "deerlantler",
    "deer_red",
    "deer_blue",
}

local tracked_prefabs =
{
    "moon_altar_astral_marker_1",
    "moon_altar_astral_marker_2",
    "moon_altar_glass",
    "moon_altar_seed",
    "moon_altar_idol",
    "moon_altar_crown",
    "moon_altar_icon",
    "moon_altar_ward",
    "moonrockseed",
    "sculpture_rooknose",
    "sculpture_bishophead",
    "sculpture_knighthead",
    "atrium_key",
    "hermit_pearl",
    "hermit_cracked_pearl",
    "lureplant",
    "dirtpile",
    "lost_toy_1",
    "lost_toy_2",
    "lost_toy_7",
    "lost_toy_10",
    "lost_toy_11",
    "lost_toy_14",
    "lost_toy_18",
    "lost_toy_19",
    "lost_toy_42",
    "lost_toy_43",
    "terrarium",
    "pirate_stash",
}

for _, recipe in pairs(AllRecipes) do
    local name = string.match(recipe.name, "^wall_(.+)_item$")
    if name then
        table.insert(wall_prefabs, "wall_"..name)
    end
end

return
{
    wall_prefabs = wall_prefabs,
    restricted_prefabs = restricted_prefabs,
    coming_bosses_prefabs = coming_bosses_prefabs,
    coming_mobs_prefabs = coming_mobs_prefabs,
    hauntable_mobs_prefabs = hauntable_mobs_prefabs,
    klaussack_prefabs = klaussack_prefabs,
    tracked_prefabs = tracked_prefabs,
}
