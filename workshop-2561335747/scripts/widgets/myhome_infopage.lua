local Widget = require "widgets/widget"
local Text = require "widgets/text"

local function SetLeftJustifiedString(txt, str)
    txt:SetString(str)
    local width, height = txt:GetRegionSize()
    txt:SetPosition(txt._position.x + width/2, txt._position.y)
end

local MyHomeInfoPage = Class(Widget, function(self, parent_screen)
    Widget._ctor(self, "MyHomeInfoPage")

    self.parent_screen = parent_screen

    self:CreatePage()
    self:_DoFocusHookups()
end)

function MyHomeInfoPage:_DoFocusHookups()
    self.parent_default_focus = self.info
end

function MyHomeInfoPage:CreatePage()
    local LINE_HEIGHT = 25
    local pos_left = -375
    local pos_top = 235

    local str_admin = STRINGS.MYHOME.PAGE_INFO_ADMIN_DISABLED
    if TUNING.MYHOME.ADMIN then
        str_admin = STRINGS.MYHOME.PAGE_INFO_ADMIN_ENABLED
    end

    local str_announce = STRINGS.MYHOME.PAGE_INFO_ANNOUNCE_DISABLED
    if TUNING.MYHOME.ANNOUNCE then
        str_announce = STRINGS.MYHOME.PAGE_INFO_ANNOUNCE_ENABLED
    end

    local str_min_walls = string.format(STRINGS.MYHOME.PAGE_INFO_MIN_WALLS, TUNING.MYHOME.MIN_WALLS)
    if TUNING.MYHOME.ADMIN then
        str_min_walls = string.format(STRINGS.MYHOME.PAGE_INFO_MIN_WALLS_ADMIN, TUNING.MYHOME.MIN_WALLS, TUNING.MYHOME.MIN_WALLS_ADMIN)
    end

    local str_max_walls = STRINGS.MYHOME.PAGE_INFO_MAX_WALLS_DISABLED
    if TUNING.MYHOME.MAX_WALLS > 0 then
        if TUNING.MYHOME.ADMIN then
            str_max_walls = string.format(STRINGS.MYHOME.PAGE_INFO_MAX_WALLS_ADMIN_ENABLED, TUNING.MYHOME.MAX_WALLS)
        else
            str_max_walls = string.format(STRINGS.MYHOME.PAGE_INFO_MAX_WALLS_ENABLED, TUNING.MYHOME.MAX_WALLS)
        end
    end

    local str_min_dist_between_myhomes = STRINGS.MYHOME.PAGE_INFO_MIN_DIST_BETWEEN_MYHOMES_DISABLED
    if TUNING.MYHOME.MIN_DIST_BETWEEN_MYHOMES > 0 then
        if TUNING.MYHOME.ADMIN then
            str_min_dist_between_myhomes = string.format(STRINGS.MYHOME.PAGE_INFO_MIN_DIST_BETWEEN_MYHOMES_ENABLED, TUNING.MYHOME.MIN_DIST_BETWEEN_MYHOMES)
        else
            str_min_dist_between_myhomes = string.format(STRINGS.MYHOME.PAGE_INFO_MIN_DIST_BETWEEN_MYHOMES_ADMIN_ENABLED, TUNING.MYHOME.MIN_DIST_BETWEEN_MYHOMES)
        end
    end

    local str_intruders = STRINGS.MYHOME.PAGE_INFO_INTRUDERS_DISABLED
    if TUNING.MYHOME.INTRUDERS then
        str_intruders = STRINGS.MYHOME.PAGE_INFO_INTRUDERS_ENABLED
    end

    local str_restricted_area = STRINGS.MYHOME.PAGE_INFO_RESTRICTED_AREA_DISABLED
    if TUNING.MYHOME.RESTRICTED_AREA then
        str_restricted_area = STRINGS.MYHOME.PAGE_INFO_RESTRICTED_AREA_ENABLED
    end

    local str_robot_separate_areas_work = STRINGS.MYHOME.PAGE_INFO_ROBOT_SEPARATE_AREAS_WORK_DISABLED
    if TUNING.MYHOME.ROBOT_SEPARATE_AREAS_WORK then
        str_robot_separate_areas_work = STRINGS.MYHOME.PAGE_INFO_ROBOT_SEPARATE_AREAS_WORK_ENABLED
    end

    local str_pushout_coming_bosses = STRINGS.MYHOME.PAGE_INFO_PUSHOUT_COMING_BOSSES_DISABLED
    if TUNING.MYHOME.PUSHOUT_COMING_BOSSES then
        str_pushout_coming_bosses = STRINGS.MYHOME.PAGE_INFO_PUSHOUT_COMING_BOSSES_ENABLED
    end

    local str_pushout_coming_mobs = STRINGS.MYHOME.PAGE_INFO_PUSHOUT_COMING_MOBS_DISABLED
    if TUNING.MYHOME.PUSHOUT_COMING_MOBS then
        str_pushout_coming_mobs = STRINGS.MYHOME.PAGE_INFO_PUSHOUT_COMING_MOBS_ENABLED
    end

    local str_pushout_hauntable_mobs = STRINGS.MYHOME.PAGE_INFO_PUSHOUT_HAUNTABLE_MOBS_DISABLED
    if TUNING.MYHOME.PUSHOUT_HAUNTABLE_MOBS then
        str_pushout_hauntable_mobs = STRINGS.MYHOME.PAGE_INFO_PUSHOUT_HAUNTABLE_MOBS_ENABLED
    end

    local str_pushout_klaussack = STRINGS.MYHOME.PAGE_INFO_PUSHOUT_KLAUSSACK_DISABLED
    if TUNING.MYHOME.PUSHOUT_KLAUSSACK then
        str_pushout_klaussack = STRINGS.MYHOME.PAGE_INFO_PUSHOUT_KLAUSSACK_ENABLED
    end

    local str_access_ruins_area = STRINGS.MYHOME.PAGE_INFO_ACCESS_RUINS_AREA_DISABLED
    if TUNING.MYHOME.ACCESS_RUINS_AREA then
        str_access_ruins_area = STRINGS.MYHOME.PAGE_INFO_ACCESS_RUINS_AREA_ENABLED
    end

    local str_access_hermitcrab_island = STRINGS.MYHOME.PAGE_INFO_ACCESS_HERMITCRAB_ISLAND_DISABLED
    if TUNING.MYHOME.ACCESS_HERMITCRAB_ISLAND then
        str_access_hermitcrab_island = STRINGS.MYHOME.PAGE_INFO_ACCESS_HERMITCRAB_ISLAND_ENABLED
    end

    local str_access_archive_maze = STRINGS.MYHOME.PAGE_INFO_ACCESS_ARCHIVE_MAZE_DISABLED
    if TUNING.MYHOME.ACCESS_ARCHIVE_MAZE then
        str_access_archive_maze = STRINGS.MYHOME.PAGE_INFO_ACCESS_ARCHIVE_MAZE_ENABLED
    end

    local str_guest_glommer_days_alive = STRINGS.MYHOME.PAGE_INFO_GUEST_GLOMMER_DAYS_ALIVE_DISABLED
    if TUNING.MYHOME.GUEST_GLOMMER_DAYS_ALIVE > 0 then
        str_guest_glommer_days_alive = string.format(STRINGS.MYHOME.PAGE_INFO_GUEST_GLOMMER_DAYS_ALIVE, TUNING.MYHOME.GUEST_GLOMMER_DAYS_ALIVE) 
    end

    local str_guest_chester_days_alive = STRINGS.MYHOME.PAGE_INFO_GUEST_CHESTER_DAYS_ALIVE_DISABLED
    if TUNING.MYHOME.GUEST_CHESTER_DAYS_ALIVE > 0 then
        str_guest_chester_days_alive = string.format(STRINGS.MYHOME.PAGE_INFO_GUEST_CHESTER_DAYS_ALIVE, TUNING.MYHOME.GUEST_CHESTER_DAYS_ALIVE) 
    end

    local str_guest_hutch_days_alive = STRINGS.MYHOME.PAGE_INFO_GUEST_HUTCH_DAYS_ALIVE_DISABLED
    if TUNING.MYHOME.GUEST_HUTCH_DAYS_ALIVE > 0 then
        str_guest_hutch_days_alive = string.format(STRINGS.MYHOME.PAGE_INFO_GUEST_HUTCH_DAYS_ALIVE, TUNING.MYHOME.GUEST_HUTCH_DAYS_ALIVE) 
    end

    local str_guest_wobot    
    if TUNING.MYHOME.FORCE_PUSHOUT_WOBOT then
        str_guest_wobot = STRINGS.MYHOME.PAGE_INFO_GUEST_WOBOT_FORCE_PUSHOUT
    else
        str_guest_wobot = STRINGS.MYHOME.PAGE_INFO_GUEST_WOBOT_DAYS_ALIVE_DISABLED
        if TUNING.MYHOME.GUEST_WOBOT_DAYS_ALIVE > 0 then
            str_guest_wobot = string.format(STRINGS.MYHOME.PAGE_INFO_GUEST_WOBOT_DAYS_ALIVE, TUNING.MYHOME.GUEST_WOBOT_DAYS_ALIVE) 
        end
    end

    self.info = self:AddChild(Widget("info"))
    self.info:SetPosition(0, 0)

    self.admin = self.info:AddChild(Text(CHATFONT, 22))
    self.admin:SetColour(UICOLOURS.GOLD)
    self.admin._position = {x = pos_left, y = pos_top}
    pos_top = pos_top - LINE_HEIGHT

    self.announce = self.info:AddChild(Text(CHATFONT, 22))
    self.announce:SetColour(UICOLOURS.GOLD)
    self.announce._position = {x = pos_left, y = pos_top}
    pos_top = pos_top - LINE_HEIGHT

    self.min_walls = self.info:AddChild(Text(CHATFONT, 22))
    self.min_walls:SetColour(UICOLOURS.GOLD)
    self.min_walls._position = {x = pos_left, y = pos_top}
    pos_top = pos_top - LINE_HEIGHT

    self.max_walls = self.info:AddChild(Text(CHATFONT, 22))
    self.max_walls:SetColour(UICOLOURS.GOLD)
    self.max_walls._position = {x = pos_left, y = pos_top}
    pos_top = pos_top - LINE_HEIGHT
    
    self.min_dist_between_myhomes = self.info:AddChild(Text(CHATFONT, 22))
    self.min_dist_between_myhomes:SetColour(UICOLOURS.GOLD)
    self.min_dist_between_myhomes._position = {x = pos_left, y = pos_top}
    pos_top = pos_top - LINE_HEIGHT

    self.intruders = self.info:AddChild(Text(CHATFONT, 22))
    self.intruders:SetColour(UICOLOURS.GOLD)
    self.intruders._position = {x = pos_left, y = pos_top}
    pos_top = pos_top - LINE_HEIGHT

    self.restricted_area = self.info:AddChild(Text(CHATFONT, 22))
    self.restricted_area:SetColour(UICOLOURS.GOLD)
    self.restricted_area._position = {x = pos_left, y = pos_top}
    pos_top = pos_top - LINE_HEIGHT
    
    self.robot_separate_areas_work = self.info:AddChild(Text(CHATFONT, 22))
    self.robot_separate_areas_work:SetColour(UICOLOURS.GOLD)
    self.robot_separate_areas_work._position = {x = pos_left, y = pos_top}
    pos_top = pos_top - LINE_HEIGHT

    self.pushout_coming_bosses = self.info:AddChild(Text(CHATFONT, 22))
    self.pushout_coming_bosses:SetColour(UICOLOURS.GOLD)
    self.pushout_coming_bosses._position = {x = pos_left, y = pos_top}
    pos_top = pos_top - LINE_HEIGHT

    self.pushout_coming_mobs = self.info:AddChild(Text(CHATFONT, 22))
    self.pushout_coming_mobs:SetColour(UICOLOURS.GOLD)
    self.pushout_coming_mobs._position = {x = pos_left, y = pos_top}
    pos_top = pos_top - LINE_HEIGHT

    self.pushout_hauntable_mobs = self.info:AddChild(Text(CHATFONT, 22))
    self.pushout_hauntable_mobs:SetColour(UICOLOURS.GOLD)
    self.pushout_hauntable_mobs._position = {x = pos_left, y = pos_top}
    pos_top = pos_top - LINE_HEIGHT

    self.pushout_klaussack = self.info:AddChild(Text(CHATFONT, 22))
    self.pushout_klaussack:SetColour(UICOLOURS.GOLD)
    self.pushout_klaussack._position = {x = pos_left, y = pos_top}
    pos_top = pos_top - LINE_HEIGHT

    self.access_ruins_area = self.info:AddChild(Text(CHATFONT, 22))
    self.access_ruins_area:SetColour(UICOLOURS.GOLD)
    self.access_ruins_area._position = {x = pos_left, y = pos_top}
    pos_top = pos_top - LINE_HEIGHT

    self.access_hermitcrab_island = self.info:AddChild(Text(CHATFONT, 22))
    self.access_hermitcrab_island:SetColour(UICOLOURS.GOLD)
    self.access_hermitcrab_island._position = {x = pos_left, y = pos_top}
    pos_top = pos_top - LINE_HEIGHT

    self.access_archive_maze = self.info:AddChild(Text(CHATFONT, 22))
    self.access_archive_maze:SetColour(UICOLOURS.GOLD)
    self.access_archive_maze._position = {x = pos_left, y = pos_top}
    pos_top = pos_top - LINE_HEIGHT

    self.guest_glommer_days_alive = self.info:AddChild(Text(CHATFONT, 22))
    self.guest_glommer_days_alive:SetColour(UICOLOURS.GOLD)
    self.guest_glommer_days_alive._position = {x = pos_left, y = pos_top}
    pos_top = pos_top - LINE_HEIGHT

    self.guest_chester_days_alive = self.info:AddChild(Text(CHATFONT, 22))
    self.guest_chester_days_alive:SetColour(UICOLOURS.GOLD)
    self.guest_chester_days_alive._position = {x = pos_left, y = pos_top}
    pos_top = pos_top - LINE_HEIGHT

    self.guest_hutch_days_alive = self.info:AddChild(Text(CHATFONT, 22))
    self.guest_hutch_days_alive:SetColour(UICOLOURS.GOLD)
    self.guest_hutch_days_alive._position = {x = pos_left, y = pos_top}
    pos_top = pos_top - LINE_HEIGHT

    self.guest_wobot = self.info:AddChild(Text(CHATFONT, 22))
    self.guest_wobot:SetColour(UICOLOURS.GOLD)
    self.guest_wobot._position = {x = pos_left, y = pos_top}
    pos_top = pos_top - LINE_HEIGHT
    
    SetLeftJustifiedString(self.admin, str_admin)
    SetLeftJustifiedString(self.announce, str_announce)
    SetLeftJustifiedString(self.min_walls, str_min_walls)
    SetLeftJustifiedString(self.max_walls, str_max_walls)
    SetLeftJustifiedString(self.min_dist_between_myhomes, str_min_dist_between_myhomes)
    SetLeftJustifiedString(self.intruders, str_intruders)
    SetLeftJustifiedString(self.restricted_area, str_restricted_area)
    SetLeftJustifiedString(self.robot_separate_areas_work, str_robot_separate_areas_work)
    SetLeftJustifiedString(self.pushout_coming_bosses, str_pushout_coming_bosses)
    SetLeftJustifiedString(self.pushout_coming_mobs, str_pushout_coming_mobs)
    SetLeftJustifiedString(self.pushout_hauntable_mobs, str_pushout_hauntable_mobs)
    SetLeftJustifiedString(self.pushout_klaussack, str_pushout_klaussack)
    SetLeftJustifiedString(self.access_ruins_area, str_access_ruins_area)
    SetLeftJustifiedString(self.access_hermitcrab_island, str_access_hermitcrab_island)
    SetLeftJustifiedString(self.access_archive_maze, str_access_archive_maze)
    SetLeftJustifiedString(self.guest_glommer_days_alive, str_guest_glommer_days_alive)
    SetLeftJustifiedString(self.guest_chester_days_alive, str_guest_chester_days_alive)
    SetLeftJustifiedString(self.guest_hutch_days_alive, str_guest_hutch_days_alive)
    SetLeftJustifiedString(self.guest_wobot, str_guest_wobot)
end

return MyHomeInfoPage
