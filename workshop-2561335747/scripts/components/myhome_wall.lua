local DummyFn = function() end
local DummyTrueFn = function() return true end
local DummyFalseFn = function() return false end

--This component runs on client as well
local MyHome_Wall = Class(function(self, inst)
    self.inst = inst
    self.ownerindex = 0
    self.lockerindex = 0
    self.homeindex = 0
    self.ownername = ""

    if not TheNet:IsDedicated() then
        self.labeltask = nil
        self.label = self.inst.entity:AddLabel()
        self.label:SetFont(DEFAULTFONT)
        self.label:SetFontSize(35)
        self.label:SetWorldOffset(0, 3, 0)
        self.label:SetColour(.5, 1, 0)
        self.label:SetText("")
        self.label:Enable(false)
    end

    self.iconfx = nil
    self.iconfxtask = nil
    self.is_ctrliconfx = false

    if TheWorld.ismastersim then
        self.forcedestroyhome_fn = function(ent)
            local home = TheWorld.net.components.myhome_manager:GetHomeByHomeIndex(ent.components.myhome_wall.homeindex)
            if home ~= nil and not home:IsProcessDestroy() then
                home:DoDestroy(nil, true)
            end
        end
        self.inst:ListenForEvent("onsink", self.forcedestroyhome_fn)
        self.inst:ListenForEvent("onfallinvoid", self.forcedestroyhome_fn)
    end
end)

--Client/Server side
function MyHome_Wall:SetOwnerByIndex(index)
    if type(index) ~= "number" then
        return
    end

    local renameowner
    if TheWorld.ismastersim then
        if self.inst:HasTag("mh_o"..self.ownerindex) then
            self.inst:RemoveTag("mh_o"..self.ownerindex)
            renameowner = true
        end
        self.inst:AddTag("mh_o"..index)
    end

    self.ownerindex = index

    if self.ownername == "" or renameowner then
        self.ownername = TheWorld.net.components.myhome_manager:GetNameByIndex(index)
        if self.label ~= nil then --nil, if dedicated server
            self.label:SetText(self.ownername)
        end
    end
end

function MyHome_Wall:SetOwnerFromTag()
    local numusers = TheWorld.net.components.myhome_manager:GetNumUsers()
    for i = 1, numusers do
        if self.inst:HasTag("mh_o"..i) then
            self.ownerindex = i

            if self.ownername == "" then
                self.ownername = TheWorld.net.components.myhome_manager:GetNameByIndex(i)
                if self.label ~= nil then --nil, if dedicated server
                    self.label:SetText(self.ownername)
                end
            end
            break
        end
    end
end

function MyHome_Wall:SetLockerFromTag()
    local numusers = TheWorld.net.components.myhome_manager:GetNumUsers()
    for i = 1, numusers do
        if self.inst:HasTag("mh_l"..i) then
            self.lockerindex = i
            break
        end
    end
end

function MyHome_Wall:ShowTooltip()
    if not TheNet:IsDedicated() and self.labeltask == nil then
        self.label:Enable(true)
        self.labeltask = self.inst:DoTaskInTime(0.2, function(ent)
            self.label:Enable(false)
            ent.components.myhome_wall.labeltask = nil
        end)
    end
end

--Server side
function MyHome_Wall:SetModeColor(mode)
    if TheWorld.ismastersim then
        local anim = self.inst.AnimState
        if self.inst:HasTag("door") and self.inst.highlightforward then
            anim = self.inst.highlightforward.AnimState
        end
        if anim ~= nil then
            if mode == 0 then
                anim:SetMultColour(1, 1, 1, 1)
                anim:SetLightOverride(0)
            else
                if mode == 1 then
                    anim:SetMultColour(.8, .8, .8, 1)
                else
                    anim:SetMultColour(.1, .1, .1, .1)
                end
                anim:SetLightOverride(3)
            end
        end
    end
end

function MyHome_Wall:Lock(homeindex, lockerindex)
    if TheWorld.ismastersim and homeindex > 0 and lockerindex > 0 then
        self.homeindex = homeindex
        self.lockerindex = lockerindex
        self.inst:AddTag("mh_lock")
        self.inst:AddTag("mh_h"..self.homeindex)
        self.inst:AddTag("mh_l"..self.lockerindex)
        self.inst:AddTag("notarget")
        self.inst:AddTag("noattack")

        local comp = self.inst.components
        if comp ~= nil then
            if comp.workable then
                comp.workable:SetWorkable(false)
                comp.workable.MH_WorkedBy = comp.workable.WorkedBy
                comp.workable.WorkedBy = DummyFn
                comp.workable.MH_workable = true
            end
            if comp.repairable then
                comp.repairable.MH_Repair = comp.repairable.Repair
                comp.repairable.Repair = DummyFalseFn
                comp.repairable.MH_repairable = true
            end
            if comp.health then
                comp.health:SetInvincible(true)
                comp.health.MH_Kill = comp.health.Kill
                comp.health.Kill = DummyFn
                comp.health.MH_DoFireDamage = comp.health.DoFireDamage
                comp.health.DoFireDamage = DummyFn
                comp.health.MH_health = true
            end
            if comp.combat then
                comp.combat.MH_GetAttacked = comp.combat.GetAttacked
                comp.combat.GetAttacked = DummyTrueFn
                comp.combat.canattack = false
                comp.combat.MH_combat = true
            end
            if comp.burnable then
                self.inst:RemoveTag("canlight")
                self.inst:AddTag("nolight")
                self.inst:AddTag("fireimmune")
                comp.burnable.extinguishimmediately = true
                if comp.burnable.flammability then
                    comp.burnable.MH_flammability = comp.burnable.flammability
                    comp.burnable.flammability = 0
                end
            end
            if comp.propagator and comp.propagator.flashpoint then
                comp.propagator.MH_flashpoint = comp.propagator.flashpoint
                comp.propagator.flashpoint = 0
            end
        end
    end
end

function MyHome_Wall:UnLock()
    if TheWorld.ismastersim then
        self.inst:RemoveTag("mh_lock")
        self.inst:RemoveTag("mh_h"..self.homeindex)
        self.inst:RemoveTag("mh_l"..self.lockerindex)
        self.inst:RemoveTag("notarget")
        self.inst:RemoveTag("noattack")

        if self.inst:HasTag("mh_ctrl") then
            self.inst:RemoveTag("mh_ctrl")
        end

        self.homeindex = 0
        self.lockerindex = 0

        local comp = self.inst.components
        if comp.workable and comp.workable.MH_workable then
            comp.workable:SetWorkable(true)
            comp.workable.MH_workable = nil
            comp.workable.WorkedBy = comp.workable.MH_WorkedBy
            comp.workable.MH_WorkedBy = nil
        end
        if comp.repairable and comp.repairable.MH_repairable then
            comp.repairable.Repair = comp.repairable.MH_Repair
            comp.repairable.MH_Repair = nil
            comp.repairable.MH_repairable = nil
        end
        if comp.health and comp.health.MH_health then
            comp.health:SetInvincible(false)
            comp.health.Kill = comp.health.MH_Kill
            comp.health.DoFireDamage = comp.health.MH_DoFireDamage
            comp.health.MH_Kill = nil
            comp.health.MH_DoFireDamage = nil
            comp.health.MH_health = nil
        end
        if comp.combat and comp.combat.MH_combat then
            comp.combat.GetAttacked = comp.combat.MH_GetAttacked
            comp.combat.canattack = true
            comp.combat.MH_GetAttacked = nil
            comp.combat.MH_combat = nil
        end
        if comp.burnable then
            self.inst:AddTag("canlight")
            self.inst:RemoveTag("nolight")
            self.inst:RemoveTag("fireimmune")
            if comp.burnable.MH_flammability ~= nil then
                comp.burnable.flammability = comp.burnable.MH_flammability
                comp.burnable.MH_flammability = nil
            end
        end
        if comp.propagator and comp.propagator.MH_flashpoint then
            comp.propagator.flashpoint = comp.propagator.MH_flashpoint
            comp.propagator.MH_flashpoint = nil
        end
    end
end

function MyHome_Wall:OnSave()
    local data = {ownerindex = self.ownerindex, homeindex = self.homeindex, lockerindex = self.lockerindex}
    if self.inst:HasTag("mh_ctrl") then
        data.ctrl = true
    end
    return data
end

function MyHome_Wall:OnLoad(data)
    if data ~= nil then
        if data.ownerindex ~= nil then
            self:SetOwnerByIndex(data.ownerindex)
        end

        if data.homeindex ~= nil then
            self.homeindex = data.homeindex
        end

        if data.lockerindex then
            --Note: self.homeindex must be inited before call Lock()
            self:Lock(self.homeindex, data.lockerindex)
        end

        if data.ctrl == true then
            self.inst:AddTag("mh_ctrl")
        end
    end
end

function MyHome_Wall:OnRemoveEntity()
    if self.homeindex > 0 then
        local home = TheWorld.net.components.myhome_manager:GetHomeByHomeIndex(self.homeindex)
        if home ~= nil then
            home:DoTaskInTime(0, function(ent)
                if not ent:IsProcessDestroy() then
                    ent:DoDestroy()
                end
            end)
        end
    end
end

function MyHome_Wall:GetDebugString()
    local lock = false
    local str = "\n\townerindex: "..self.ownerindex.."\n\t"

    str = str.."ownername: "..tostring(self.ownername).."\n\t"

    if TheWorld.ismastersim then
        str = str.."homeindex: "..self.homeindex.."\n\t"
        str = str.."lockerindex: "..self.lockerindex.."\n\t"
    end

    if self.inst:HasTag("mh_lock") then
        lock = true
    end

    str = str.."lock: "..tostring(lock)

    return str
end

return MyHome_Wall
