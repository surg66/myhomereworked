local _G = GLOBAL

local function pickup(inst, owner)
    local item = _G.FindPickupableItemMyHome(owner, _G.TUNING.ORANGEAMULET_RANGE, false)
    if item == nil then
        return
    end

    local didpickup = false
    if item.components.trap ~= nil then
        item.components.trap:Harvest(owner)
        didpickup = true
    end

    if owner.components.minigame_participator ~= nil then
        local minigame = owner.components.minigame_participator:GetMinigame()
        if minigame ~= nil then
            minigame:PushEvent("pickupcheat", { cheater = owner, item = item })
        end
    end

    --Amulet will only ever pick up items one at a time. Even from stacks.
    _G.SpawnPrefab("sand_puff").Transform:SetPosition(item.Transform:GetWorldPosition())

    inst.components.finiteuses:Use(1)

    if not didpickup then
        local item_pos = item:GetPosition()
        if item.components.stackable ~= nil then
            item = item.components.stackable:Get()
        end

        owner.components.inventory:GiveItem(item, nil, item_pos)
    end
end


local function onequip_orange(inst, owner)
    owner.AnimState:OverrideSymbol("swap_body", "torso_amulets", "orangeamulet")
    inst.task = inst:DoPeriodicTask(_G.TUNING.ORANGEAMULET_ICD, pickup, nil, owner)
end

AddPrefabPostInit("orangeamulet", function(inst)
    if not _G.TheWorld.ismastersim then
        return inst
    end

    inst.components.equippable:SetOnEquip(onequip_orange)
end)
