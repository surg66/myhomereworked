local _G = GLOBAL
local require = _G.require

-- Server only ------------------------------------------------------------
if not _G.TheNet:GetIsClient() then
    if _G.TUNING.MYHOME.RESTRICTED_AREA then
        AddPrefabPostInit("tigershark", function(inst)
            inst:AddTag("mh_r1")
        end)

        AddPrefabPostInit("sharkittenspawner", function(inst)
            inst:AddTag("mh_r2")
        end)

        AddPrefabPostInit("packim_fishbone_spawner", function(inst)
            inst:AddTag("mh_r2")
        end)
    end
end -- end Server only
