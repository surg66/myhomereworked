local _G = GLOBAL

local function ActionTouchWall(act)
    if act.doer ~= nil  and
       act.target ~= nil and act.target.components.myhome_wall ~= nil then
        local userindex = _G.TheWorld.net.components.myhome_manager:GetUserIndexByPlayer(act.doer)
        local ownerindex = act.target.components.myhome_wall.ownerindex

        if ownerindex > 0 and act.doer.components.talker ~= nil then
            local myhome_manager = _G.TheWorld.net.components.myhome_manager
            local ownername = myhome_manager:GetNameByIndex(ownerindex)

            if userindex > 0 and myhome_manager:IsFriend(ownerindex, userindex) then
                act.doer.components.talker:Say(string.format(_G.STRINGS.MYHOME.WALL_FRIEND, ownername), 3)
            else
                local scoreindex = _G.tostring(myhome_manager:GetScoreIndexByPlayer(act.doer))
                act.doer.components.talker:Say(string.format(_G.STRINGS.MYHOME.WALL_ANOTHER, ownername, scoreindex), 3)
            end
        end
    end
end

local function ActionShowCtrl(act)
    if act.doer ~= nil  and
       act.target ~= nil and act.target.components.myhome_wall ~= nil then
        _G.TheWorld.net.components.myhome_manager:DoShowCtrl(act.target, act.doer)
    end
end

local mh_actions = {}

mh_actions["lock"] = AddAction("MH_LOCK", _G.STRINGS.MYHOME.LOCK, function(act)
    if act.doer ~= nil  and
       act.target  ~= nil and act.target.components.myhome_wall ~= nil then
        _G.TheWorld.net.components.myhome_manager:DoBuild(act.target, act.doer)
    end
    return true
end)

mh_actions["unlock"] = AddAction("MH_UNLOCK", _G.STRINGS.MYHOME.UNLOCK, function(act)
    if act.doer ~= nil and
       act.target ~= nil and act.target.components.myhome_wall ~= nil then
        _G.TheWorld.net.components.myhome_manager:DoDestroy(act.target, act.doer)
    end
    return true
end)

mh_actions["focuswall"] = AddAction("MH_FOCUSWALL", "", function(act)
    if act.doer ~= nil and
       act.target ~= nil and act.target.components.myhome_wall ~= nil then
        if act.doer.components.playercontroller == nil or
            not act.doer.components.playercontroller.directwalking then
            act.doer.components.locomotor:Stop()
        end
        _G.TheWorld.net.components.myhome_manager:DoFocusWall(act.target, act.doer)
    end
    return true
end)

mh_actions["showctrl"] = AddAction("MH_SHOWCTRL", _G.STRINGS.MYHOME.THIS_IS_HOME_MY, function(act)
    ActionShowCtrl(act)
    return true
end)

mh_actions["friend"] = AddAction("MH_FRIEND", _G.STRINGS.MYHOME.THIS_IS_WALL_FRIEND, function(act)
    ActionTouchWall(act)
    return true
end)

mh_actions["friendlocked"] = AddAction("MH_FRIENDLOCKED", _G.STRINGS.MYHOME.THIS_IS_HOME_FRIEND, function(act)
    ActionShowCtrl(act)
    return true
end)

mh_actions["another"] = AddAction("MH_ANOTHER", _G.STRINGS.MYHOME.THIS_IS_WALL_ANOTHER, function(act)
    ActionTouchWall(act)
    return true
end)

mh_actions["anotherlocked"] = AddAction("MH_ANOTHERLOCKED", _G.STRINGS.MYHOME.THIS_IS_HOME_ANOTHER, function(act)
    ActionShowCtrl(act)
    return true
end)

--NOTE: Priority for actions MH_LOCK, MH_UNLOCK, MH_FOCUSWALL, MH_SHOWCTRL, MH_FRIEND, MH_FRIENDLOCKED, MH_ANOTHER, MH_ANOTHERLOCKED
--      must be higher than ACTIVATE and action MH_FOCUSWALL higher than LOOKAT.
--      Sometimes priorities change (look actions.lua file)
--          ACTIVATE = 2 for open/close door
--          LOOKAT  = -3 for examine wall

for _, v in pairs(mh_actions) do
    v.priority = 3
end

mh_actions["focuswall"].instant = true
mh_actions["focuswall"].stroverridefn = function(act)
    local str = _G.STRINGS.MYHOME.FOCUS_WALL
    if not _G.TheInput:ControllerAttached() then
        str = _G.STRINGS.LMB..": ".._G.STRINGS.MYHOME.FOCUS_WALL
    end
    return str
end

AddComponentAction("SCENE", "myhome_wall", function(inst, doer, actions, right)
    if not right then
        if not _G.TheWorld.ismastersim then
            if inst.components.myhome_wall.ownerindex == 0 then
                inst.components.myhome_wall:SetOwnerFromTag()
            end
            if inst:HasTag("mh_lock") and not inst:HasTag("mh_l"..inst.components.myhome_wall.lockerindex) then
                inst.components.myhome_wall:SetLockerFromTag()
            end
        end

        inst.components.myhome_wall:ShowTooltip()

        if inst.components.myhome_wall.ownerindex > 0 then
            local userindex = doer.myhome_userindex
            if userindex == nil then
                userindex = _G.TheWorld.net.components.myhome_manager:GetUserIndexFromTag(doer)
                if userindex > 0 then
                    doer.myhome_userindex = userindex
                end
            end
            if userindex ~= nil and userindex > 0 and doer:HasTag("mh_captor") and
               inst:HasTag("mh_guide") and inst:HasTag("mh_capture") and inst:HasTag("mh_c"..userindex) then
                table.insert(actions, _G.ACTIONS.MH_FOCUSWALL)
            end
        end
    else
        --pass, if right
        local ownerindex = inst.components.myhome_wall.ownerindex
        if not doer:HasTag("mh_captor") and ownerindex > 0 then
            if doer.myhome_userindex ~= nil and doer.myhome_userindex > 0 then

                local ismy = (ownerindex == doer.myhome_userindex)
                local isfriend = _G.TheWorld.net.components.myhome_manager:IsFriend(ownerindex, doer.myhome_userindex)

                if inst:HasTag("mh_lock") then
                    local lockerindex = inst.components.myhome_wall.lockerindex
                    ismy = (lockerindex == doer.myhome_userindex)
                    isfriend = _G.TheWorld.net.components.myhome_manager:IsFriend(lockerindex, doer.myhome_userindex)

                    if TUNING.MYHOME.ADMIN and doer.Network:IsServerAdmin() then
                        table.insert(actions, _G.ACTIONS.MH_UNLOCK)
                    elseif ismy then
                        if inst:HasTag("mh_ctrl") then
                            table.insert(actions, _G.ACTIONS.MH_UNLOCK)
                        else
                            table.insert(actions, _G.ACTIONS.MH_SHOWCTRL)
                        end
                    elseif isfriend then
                        table.insert(actions, _G.ACTIONS.MH_FRIENDLOCKED)
                    else
                        table.insert(actions, _G.ACTIONS.MH_ANOTHERLOCKED)
                    end
                else
                    if TUNING.MYHOME.ADMIN and doer.Network:IsServerAdmin() then
                        if not inst:HasTag("mh_focus") then
                            table.insert(actions, _G.ACTIONS.MH_LOCK)
                        end
                    elseif ismy then
                        if not inst:HasTag("mh_focus") then
                            table.insert(actions, _G.ACTIONS.MH_LOCK)
                        end
                    elseif isfriend then
                        table.insert(actions, _G.ACTIONS.MH_FRIEND)
                    else
                        table.insert(actions, _G.ACTIONS.MH_ANOTHER)
                    end
                end
            else
                if inst:HasTag("mh_lock") then
                    table.insert(actions, _G.ACTIONS.MH_ANOTHERLOCKED)
                else
                    table.insert(actions, _G.ACTIONS.MH_ANOTHER)
                end
            end
        end
    end
end)

AddStategraphActionHandler("wilson",        _G.ActionHandler(_G.ACTIONS.MH_LOCK, "give"))
AddStategraphActionHandler("wilson_client", _G.ActionHandler(_G.ACTIONS.MH_LOCK, "give"))
AddStategraphActionHandler("wilson",        _G.ActionHandler(_G.ACTIONS.MH_UNLOCK, "give"))
AddStategraphActionHandler("wilson_client", _G.ActionHandler(_G.ACTIONS.MH_UNLOCK, "give"))
AddStategraphActionHandler("wilson",        _G.ActionHandler(_G.ACTIONS.MH_FOCUSWALL, "give"))
AddStategraphActionHandler("wilson_client", _G.ActionHandler(_G.ACTIONS.MH_FOCUSWALL, "give"))
AddStategraphActionHandler("wilson",        _G.ActionHandler(_G.ACTIONS.MH_SHOWCTRL, "give"))
AddStategraphActionHandler("wilson_client", _G.ActionHandler(_G.ACTIONS.MH_SHOWCTRL, "give"))
AddStategraphActionHandler("wilson",        _G.ActionHandler(_G.ACTIONS.MH_FRIEND, "give"))
AddStategraphActionHandler("wilson_client", _G.ActionHandler(_G.ACTIONS.MH_FRIEND, "give"))
AddStategraphActionHandler("wilson",        _G.ActionHandler(_G.ACTIONS.MH_FRIENDLOCKED, "give"))
AddStategraphActionHandler("wilson_client", _G.ActionHandler(_G.ACTIONS.MH_FRIENDLOCKED, "give"))
AddStategraphActionHandler("wilson",        _G.ActionHandler(_G.ACTIONS.MH_ANOTHER, "give"))
AddStategraphActionHandler("wilson_client", _G.ActionHandler(_G.ACTIONS.MH_ANOTHER, "give"))
AddStategraphActionHandler("wilson",        _G.ActionHandler(_G.ACTIONS.MH_ANOTHERLOCKED, "give"))
AddStategraphActionHandler("wilson_client", _G.ActionHandler(_G.ACTIONS.MH_ANOTHERLOCKED, "give"))
