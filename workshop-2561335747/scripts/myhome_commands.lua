local _G = GLOBAL

AddUserCommand("myhomeaddfriend", {
    prettyname = "MyHome add friend.",
    desc = "MyHome add friend.",
    permission = _G.COMMAND_PERMISSION.USER,
    slash = true,
    usermenu = false,
    servermenu = false,
    params = {"userid"},
    vote = false,
    serverfn = function(params, caller)
        local target_userid = nil
        if params.userid ~= nil then
            target_userid = _G.tostring(params.userid)
        end

        if target_userid ~= nil and caller ~= nil and caller:IsValid() then
            _G.TheWorld.net.components.myhome_manager:CommandFriends("add", caller, target_userid)
        end
    end,
})

AddUserCommand("myhomedelfriend", {
    prettyname = "MyHome delete friend.",
    desc = "MyHome delete friend.",
    permission = _G.COMMAND_PERMISSION.USER,
    slash = true,
    usermenu = false,
    servermenu = false,
    params = {"userid"},
    vote = false,
    serverfn = function(params, caller)
        local target_userid = nil
        if params.userid ~= nil then
            target_userid = _G.tostring(params.userid)
        end

        if target_userid ~= nil and caller ~= nil and caller:IsValid() then
            _G.TheWorld.net.components.myhome_manager:CommandFriends("del", caller, target_userid)
        end
    end,
})
