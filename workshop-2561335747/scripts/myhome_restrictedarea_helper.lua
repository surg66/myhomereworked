local _G = GLOBAL
local require = _G.require
local prefabdefs = require("myhome_prefabdefs")

local function OnUpdateRestrictedArea(inst)
    if inst and inst.observ and inst.observ:IsValid() and inst.observ.Transform then
        local x, y, z = inst.observ.Transform:GetWorldPosition()
        inst.Transform:SetPosition(x, y, z)
    end
end

local function OnUpdateMarker(inst)
    if inst and inst.observ and inst.observ:IsValid() and inst.observ.Transform then
        local x, y, z = inst.observ.Transform:GetWorldPosition()
        inst.Transform:SetPosition(x, y + 0.5, z)
    end
end

local function CreateMarker(inst, typerestrict)
    if inst.restrict_marker == nil then
        inst.restrict_marker = _G.CreateEntity()
        --[[Non-networked entity]]
        inst.restrict_marker.entity:SetCanSleep(false)
        inst.restrict_marker.persists = false
        inst.restrict_marker.entity:AddTransform()
        inst.restrict_marker.entity:AddAnimState()
        inst.restrict_marker:AddTag("CLASSIFIED")
        inst.restrict_marker:AddTag("NOCLICK")
        inst.restrict_marker.observ = inst
        inst.restrict_marker.Transform:SetScale(1, 1, 1)
        local x, y, z = inst.Transform:GetWorldPosition()
        inst.restrict_marker.Transform:SetPosition(x, y + 0.5, z)
        inst.restrict_marker.AnimState:SetBank("myhome_markers")
        inst.restrict_marker.AnimState:SetBuild("myhome_markers")
        inst.restrict_marker.AnimState:PlayAnimation("marker")
        if typerestrict == 1 then
            inst.restrict_marker.AnimState:OverrideSymbol("swap_iconsmall", "myhome_markers", "icon1")
        elseif typerestrict == 4 then
            inst.restrict_marker.AnimState:OverrideSymbol("swap_iconsmall", "myhome_markers", "icon3")
        else
            inst.restrict_marker.AnimState:OverrideSymbol("swap_iconsmall", "myhome_markers", "icon2")
        end
        inst.restrict_marker.AnimState:SetLightOverride(1)
        inst.restrict_marker:AddComponent("updatelooper")
        inst.restrict_marker.components.updatelooper:AddOnUpdateFn(OnUpdateMarker)
        OnUpdateMarker(inst.restrict_marker)
    end
end

local function CreateRadius(inst, typerestrict)
    if inst.restrict_radius == nil then
        inst.restrict_radius = _G.CreateEntity()
        --[[Non-networked entity]]
        inst.restrict_radius.entity:SetCanSleep(false)
        inst.restrict_radius.persists = false
        inst.restrict_radius.entity:AddTransform()
        inst.restrict_radius.entity:AddAnimState()
        inst.restrict_radius:AddTag("CLASSIFIED")
        inst.restrict_radius:AddTag("NOCLICK")
        inst.restrict_radius:AddTag("placer")
        inst.restrict_radius.observ = inst
        inst.restrict_radius.Transform:SetScale(1, 1, 1)
        local x, y, z = inst.Transform:GetWorldPosition()
        inst.restrict_radius.Transform:SetPosition(x, y, z)
        inst.restrict_radius.AnimState:SetBank("myhome_restrictedareas")
        inst.restrict_radius.AnimState:SetBuild("myhome_restrictedareas")
        if typerestrict == 3 then
            inst.restrict_radius.AnimState:PlayAnimation("rad4tiles")
        else
            inst.restrict_radius.AnimState:PlayAnimation("rad2tiles")
        end
        inst.restrict_radius.AnimState:SetLightOverride(1)
        inst.restrict_radius.AnimState:SetOrientation(_G.ANIM_ORIENTATION.OnGround)
        inst.restrict_radius.AnimState:SetLayer(_G.LAYER_BACKGROUND)
        inst.restrict_radius.AnimState:SetSortOrder(1)
        inst.restrict_radius.AnimState:OverrideMultColour(.95, .58, .11, 1)
        inst.restrict_radius:AddComponent("updatelooper")
        inst.restrict_radius.components.updatelooper:AddOnUpdateFn(OnUpdateRestrictedArea)
        OnUpdateRestrictedArea(inst.restrict_radius)
    end
end

local function OnEnableRestrictHelper(inst, enabled, recipename, placerinst, typerestrict)
    if enabled then
        CreateMarker(inst, typerestrict)
        if typerestrict == 2 or typerestrict == 3 then
            CreateRadius(inst, typerestrict)
        end
    else
        if inst.restrict_marker ~= nil then
            inst.restrict_marker.components.updatelooper:RemoveOnUpdateFn(OnUpdateMarker)
            inst.restrict_marker:Remove()
            inst.restrict_marker = nil
        end
        if inst.restrict_radius ~= nil then
            inst.restrict_radius.components.updatelooper:RemoveOnUpdateFn(OnUpdateRestrictRadius)
            inst.restrict_radius:Remove()
            inst.restrict_radius = nil
        end
    end
end

for i, tbl in ipairs(prefabdefs.restricted_prefabs) do
    for _, v in ipairs(tbl) do
        AddPrefabPostInit(v, function(inst)
            if not inst.components.deployhelper then
                inst:AddComponent("deployhelper")
                inst.components.deployhelper:AddKeyFilter("myhomewall")
                inst.components.deployhelper.onenablehelper = function(inst, enabled, recipename, placerinst)
                    OnEnableRestrictHelper(inst, enabled, recipename, placerinst, i)
                end
            end
        end)
    end
end
