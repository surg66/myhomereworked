--Note, actions override for:
--      LIGHT, CHOP, DIG, MINE, HAMMER, CASTSPELL, HAUNT, 
--      READ, TERRAFORM, DEPLOY, BUILD, BLINK, BLINK_MAP
--      ACTIVATE, ROTATE_FENCE

local _G = GLOBAL
local require = _G.require
local prefabdefs = require("myhome_prefabdefs")

local function IsWallRecipe(name)
    for _, v in ipairs(prefabdefs.wall_prefabs) do
        if v == name then
            return true
        end
    end
    return false
end

local function ScheduleTalkerSay(talker, message, second)
    talker:DoTaskInTime(0, function(ent)
        if ent and ent:IsValid() and ent.components.talker ~= nil then
            ent.components.talker:Say(message, second or 3)
        end
    end)
end

local function TestActionInside(x, y, z, doer)
    local home = _G.TheWorld.net.components.myhome_manager:GetHomeByPosition(x, y, z)
    if home ~= nil and not home:IsAvailableForDoer(doer) then
        ScheduleTalkerSay(doer, _G.STRINGS.MYHOME.THIS_IS_HOME_ANOTHER)
        return false
    end
    return true
end

local function TypicalActionRange(x, y, z, range, doer)
    --pass1, test inside
    if not TestActionInside(x, y, z, doer) then
        return false
    end
    --pass2, test outside (or inside, if wall near)
    local ents = _G.TheSim:FindEntities(x, y, z, range, {"mh_lock"})
    for _, wall in ipairs(ents) do
        local homeindex = wall.components.myhome_wall.homeindex
        if homeindex > 0 then
            local home = _G.TheWorld.net.components.myhome_manager:GetHomeByHomeIndex(homeindex)
            if home ~= nil and not home:IsAvailableForDoer(doer) then
                ScheduleTalkerSay(doer, _G.STRINGS.MYHOME.THIS_IS_HOME_ANOTHER)
                return false
            end
        end
    end

    return true
end

local original_actions_fn = {}
for action, range in pairs({CHOP = 8, DIG = 8, MINE = 8, HAMMER = 8, CASTSPELL = 8, HAUNT = 8, READ = 8}) do
    original_actions_fn[action] = _G.ACTIONS[action].fn

    _G.ACTIONS[action].fn = function(act)
        local additional_range = 0
        local pos = nil

        if act.target ~= nil and act.target:IsValid() then
            pos = act.target:GetPosition()
            if action == "DIG" and act.target.prefab == "pirate_stash" then
                return original_actions_fn[action](act) -- skip check myhome
            end
        elseif act.pos ~= nil then
            pos = act.pos:GetPosition()
        else
            pos = act.doer:GetPosition()
        end

        if act.invobject and act.invobject.prefab == "book_tentacles" then
            additional_range = 4
        end

        if pos ~= nil and not TypicalActionRange(pos.x, pos.y, pos.z, range + additional_range, act.doer) then
            return false
        end

        return original_actions_fn[action](act)
    end
end

original_actions_fn.LIGHT = _G.ACTIONS.LIGHT.fn
_G.ACTIONS.LIGHT.fn = function(act)
    local pos = nil

    if act.target ~= nil and act.target:IsValid() then
        pos = act.target:GetPosition()
    elseif act.pos ~= nil then
        pos = act.pos:GetPosition()
    else
        pos = act.doer:GetPosition()
    end

    if pos ~= nil then
        if act.target ~= nil and act.target.prefab == "miniflare" or act.target.prefab == "megaflare" then
            if not TestActionInside(pos.x, pos.y, pos.z, act.doer) then
                return false
            end
        elseif not TypicalActionRange(pos.x, pos.y, pos.z, 8, act.doer) then
            return false
        end
    end

    return original_actions_fn.LIGHT(act)
end

original_actions_fn.TERRAFORM = _G.ACTIONS.TERRAFORM.fn
_G.ACTIONS.TERRAFORM.fn = function(act)
    if act.pos ~= nil then
        local pos = act.pos:GetPosition()
        if not TypicalActionRange(pos.x, pos.y, pos.z, 8, act.doer) then
            return false
        end
    end

    return original_actions_fn.TERRAFORM(act)
end

original_actions_fn.BUILD = _G.ACTIONS.BUILD.fn
_G.ACTIONS.BUILD.fn = function(act)
    if act.pos ~= nil and act.recipe ~= nil then
        local recipe = _G.AllRecipes[act.recipe]
        if recipe and recipe.placer ~= nil and
           recipe.name ~= "campfire" and recipe.name ~= "coldfire" and
           not IsWallRecipe(recipe.name) then
            local pos = act.pos:GetPosition()
            if pos ~= nil and not TypicalActionRange(pos.x, pos.y, pos.z, 8, act.doer) then
                return false
            end
        end
    end

    return original_actions_fn.BUILD(act)
end

original_actions_fn.DEPLOY = _G.ACTIONS.DEPLOY.fn
_G.ACTIONS.DEPLOY.fn = function(act)
    if act.pos ~= nil and act.invobject.prefab ~= "archive_resonator_item" and act.invobject.prefab ~= "wx78_scanner_item" then
        local pos = act.pos:GetPosition()
        if pos ~= nil and not TypicalActionRange(pos.x, pos.y, pos.z, 8, act.doer) then
            return false
        end
    end

    return original_actions_fn.DEPLOY(act)
end

original_actions_fn.BLINK = _G.ACTIONS.BLINK.fn
_G.ACTIONS.BLINK.fn = function(act)
    if act.pos ~= nil then
        local pos = act.pos:GetPosition()
        local home = _G.TheWorld.net.components.myhome_manager:GetHomeByPosition(pos.x, pos.y, pos.z)
        if home ~= nil and not home:IsAvailableForDoer(act.doer) then
            ScheduleTalkerSay(act.doer, _G.STRINGS.MYHOME.THIS_IS_HOME_ANOTHER)
            return false
        end
    end

    return original_actions_fn.BLINK(act)
end

original_actions_fn.BLINK_MAP = _G.ACTIONS.BLINK_MAP.fn
_G.ACTIONS.BLINK_MAP.fn = function(act)
    if act.pos ~= nil then
        local pos = act.pos:GetPosition()
        local home = _G.TheWorld.net.components.myhome_manager:GetHomeByPosition(pos.x, pos.y, pos.z)
        if home ~= nil and not home:IsAvailableForDoer(act.doer) then
            ScheduleTalkerSay(act.doer, _G.STRINGS.MYHOME.THIS_IS_HOME_ANOTHER)
            return false
        end
    end

    return original_actions_fn.BLINK_MAP(act)
end

original_actions_fn.ACTIVATE = _G.ACTIONS.ACTIVATE.fn
_G.ACTIONS.ACTIVATE.fn = function(act)
    if act.target ~= nil and act.target:IsValid() and act.target:HasTag("mh_lock") and act.target.components.myhome_wall ~= nil then
        local homeindex = act.target.components.myhome_wall.homeindex
        if homeindex > 0 then
            local home = _G.TheWorld.net.components.myhome_manager:GetHomeByHomeIndex(homeindex)
            if home ~= nil and not home:IsAvailableForDoer(act.doer) then
                ScheduleTalkerSay(act.doer, _G.STRINGS.MYHOME.THIS_IS_HOME_ANOTHER)
                return false
            end
        end
    end

    return original_actions_fn.ACTIVATE(act)
end

original_actions_fn.ROTATE_FENCE = _G.ACTIONS.ROTATE_FENCE.fn
_G.ACTIONS.ROTATE_FENCE.fn = function(act)
    if act.target ~= nil and act.target:IsValid() and act.target:HasTag("mh_lock") and act.target.components.myhome_wall ~= nil then
        local homeindex = act.target.components.myhome_wall.homeindex
        if homeindex > 0 then
            local home = _G.TheWorld.net.components.myhome_manager:GetHomeByHomeIndex(homeindex)
            if home ~= nil then
                if not home:IsAvailableForDoer(act.doer) then
                    ScheduleTalkerSay(act.doer, _G.STRINGS.MYHOME.THIS_IS_HOME_ANOTHER)
                end
                --NOTE: if available for doer then talk nothing
                return false
            end
        end
    end

    return original_actions_fn.ROTATE_FENCE(act)
end

original_actions_fn.CASTAOE = _G.ACTIONS.CASTAOE.fn
_G.ACTIONS.CASTAOE.fn = function(act)
    if act.pos ~= nil then
        local pos = act.pos:GetPosition()
        local home = _G.TheWorld.net.components.myhome_manager:GetHomeByPosition(pos.x, pos.y, pos.z)
        if home ~= nil and not home:IsAvailableForDoer(act.doer) then
            ScheduleTalkerSay(act.doer, _G.STRINGS.MYHOME.THIS_IS_HOME_ANOTHER)
            return false
        end
    end

    return original_actions_fn.CASTAOE(act)
end
